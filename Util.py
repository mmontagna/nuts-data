import MySQLdb
import os
import time
import simplejson

class DB:
    def __init__(self):
        self.db = MySQLdb.connect(host=os.environ['db_host'],
                                user=os.environ['db_user'],
                                passwd=os.environ['db_password'],
                                db=os.environ['db_db'],
                                use_unicode=True,
                                charset='utf8',
                                )
        self.cur = self.db.cursor()
    def fetch(self, sql):
        self.cur.execute(sql)
        return [list(row) for row in self.cur.fetchall()]

class RecordFieldSet:
    def __init__(self, fields):
        self.fields = {}
        self.orderedFields = fields
        self.records = []
        #Map field names to indices in a dictionary.
        #Allows for fast lookup of field position
        for i in range(0, len(fields)):
            self.fields[fields[i]] = i
    def createRecord(self, values):
        #r = Record(self, values)
        r = (self, values)
        self.records.append(r)
        return r
    @staticmethod
    def get(record, key):
        i = record[0].fieldPosition(key)
        return record[1][i]
    @staticmethod
    def set(record, key, val):
        i = record[0].fieldPosition(key)
        if (i == None):
            i = record[0].addField(key)
        record[1][i] = val
    @staticmethod
    def printListJson(records, outBuffer):
        outBuffer.append('[')
        for record in records:
            RecordFieldSet.printRecordJson(record, outBuffer)
            if record != records[-1]:
                outBuffer.append(',')
        outBuffer.append(']')
    @staticmethod
    def printRecordJson(record, outBuffer):

        outBuffer.append('{')
        for field in record[0].orderedFields:
            outBuffer.append('"' + field + '":')
            value = RecordFieldSet.get(record, field)
            if (isinstance(value, tuple)):
                RecordFieldSet.printRecordJson(value, outBuffer)
            elif (isinstance(value, list)):
                RecordFieldSet.printListJson(value, outBuffer)
            else:
                outBuffer.append(simplejson.dumps(value))
            if (field != record[0].orderedFields[-1]):
                outBuffer.append(',')
        outBuffer.append('}')

    def fieldPosition(self, field):
        if (field in self.fields):
            return self.fields[field]
        return None
    def addField(self, field):
        i = len(self.orderedFields)
        self.orderedFields.append(field)
        self.fields[field] = i
        for record in self.records:
            record[1].append("")
        return i

class OutputBuffer:
    def __init__(self, filename):
        self.buffer = []
        self.bufferSize = 1000
        self.outfile = open(filename, 'w')
    def close(self):
        self.outfile.close()
    def flush(self):
        self.outfile.write("".join(self.buffer))
        self.buffer = []
    def append(self, obj):
        self.buffer.append(obj)
        if (len(self.buffer) > self.bufferSize):
            self.flush()

class Exporter:
    db = DB()
    def __init__(self, table):
        self.db = Exporter.db
        self.table = table
        self.blackList = []
        self.whiteList = []
        self.fieldFilterMode = 'blacklist'
        self.mappedFields = {}
        self.records = None

    def processField(self, field):
        field = field.lower().replace('_', '')
        return self.mappedFields[field] if field in self.mappedFields else field
    def exclude(self, fields):
        self.fieldFilterMode = 'blacklist'
        self.blackList = [self.processField(field) for field in fields]
    def use(self, fields):
        self.fieldFilterMode = 'whitelist'
        self.whiteList = [self.processField(field) for field in fields]
    def mapField(self, originalName, newName):
        self.mappedFields[originalName] = newName
    def includeField(self, field):
        field = self.processField(field)
        if (self.fieldFilterMode == 'blacklist'):
            return not field in self.blackList
        if (self.fieldFilterMode == 'whitelist'):
            return field in self.whiteList
    def _get(self):
        if (self.records == None):
            cols = [row[0] for row in self.db.fetch("SHOW columns FROM " + self.table)]
            i = 0
            while i < len(cols):
                if (not self.includeField(cols[i])):
                    cols.pop(i)
                else:
                    i = i + 1
            start = time.time()
            records = self.db.fetch("SELECT "+ '`' + '`,`'.join(cols) + '`' +" FROM " + self.table)
            end = time.time()
            print "retrieved db rows in ", end - start, "s"
            start = time.time()
            self.recordFieldSet = RecordFieldSet([self.processField(row) for row in cols])
            print "field set built in ", time.time() - start, "s"

            self.records = [self.recordFieldSet.createRecord(row) for row in records]
            end = time.time()
            print "build record set n i", end - start, "s"

    def export(self):
        self._get()
        return self.records

    def writeJson(self, filename = 'output.json'):
        self._get()
        out = OutputBuffer(filename)
        RecordFieldSet.printListJson(self.records, out)
        out.flush()
        out.close()
    def mergeOn(self, field, resultsRecords, field2):
        print "merging ", field2, " into ", field

        self._get()
        field = self.processField(field)
        field2 = self.processField(field2)
        start = time.time()
        mergeSet = {}
        for mergeRecord in resultsRecords:
            key = RecordFieldSet.get(mergeRecord, field2)
            mergeSet[key] = mergeRecord

        end = time.time()
        print "Merge set built in ",  end - start, "s"
        start = time.time()
        for record in self.records:
            if RecordFieldSet.get(record, field) in mergeSet:
                RecordFieldSet.set(record, field, mergeRecord)
        end = time.time()
        print "Merged in ",  end - start, "s"
    def collect(self, bin, field, records, field2):
        self._get()
        field = self.processField(field)
        field2 = self.processField(field2)
        start = time.time()
        mergeSet = {}

        for target in self.records:
            mergeSet[RecordFieldSet.get(target, field)] = target
            RecordFieldSet.set(target, bin, [])

        for record in records:
            if RecordFieldSet.get(record, field2) in mergeSet:
                key = RecordFieldSet.get(record, field2)
                RecordFieldSet.get(mergeSet[key], bin).append(record)
