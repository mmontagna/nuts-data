#!/usr/bin/python
from Util import *
import sys

#Note all fields are converted to lower case and underscores stripped


UserExporter = Exporter('user')
UserExporter.use(['id', 'Name', 'Email', 'Address'])


AddressExporter = Exporter('address')
AddressExporter.mapField('phone1', 'phone')
AddressExporter.use(['id', 'City', 'StateProv', 'Residential', 'phone'])


print "Exporting addresses"
addresses = AddressExporter.export()

print "Exporting users"
users = UserExporter.export()

# print "Merge addresses into users"
UserExporter.mergeOn('address', addresses, 'id')

print "Exporting Invoices"
InvoiceBillingExporter = Exporter('invoice_billing')
InvoiceBillingExporter.use(['id', 'invoice', 'charge', 'creditcard'])


InvoiceExporter = Exporter('invoice')
InvoiceExporter.use(['id', 'user'])
InvoiceExporter.collect('billing', 'id', InvoiceBillingExporter.export(), 'invoice')

invoices = InvoiceExporter.export()

print "Collect invoices into users"
UserExporter.collect('orders', 'id', invoices, 'user')


UserExporter.writeJson()

#print users